import React from "react";

const RateRow = ({rate,baseCurrency})=>{
  return (
    <div>
      {(baseCurrency.code===rate[0])
      ?
      <span>{rate[0]}:{rate[1]}</span> 
      :
        <span>{rate[0]}:{rate[1]}</span>
      }
    </div> 
  )
}

export default RateRow;
