import React from "react";
import ForexRates from "./ForexRates";

const ForexExchange = ({baseCurrencyRates,baseCurrency})=>{
  return(
    <div
      className="d-flex flex-column"
    >
      {baseCurrencyRates.map((baseCurrencyRate,index)=>{
        return(
          <ForexRates
            key={index}
            rate={baseCurrencyRate}
            baseCurrency={baseCurrency}
          />
        )
      })}
    </div>
  )
}

export default ForexExchange;
