import React, { Component } from 'react';
// import box component
import Box from './components/Box';
// import buttons component
import Buttons from './components/Buttons';

class App extends Component {

  state = { 
    count:0
   }

   handleAdd = () => {
    this.setState({
      count : this.state.count + 1
    });

   }

   handleMinus = () => {
     this.setState({
       count : this.state.count - 1
     });
   }

   handleReset = () => {
     this.setState({
       count : 0
     });
   }

   handleMultifly = () => {
     this.setState({
       count: this.state.count * 2
     });
   }
  render() { 
    return ( 
      //  A React objects
      <React.Fragment>
        <Box 
        count = {this.state.count}/>
        
        <Buttons 
        handleAdd = {this.handleAdd}
        handleMinus = {this.handleMinus}
        handleMultifly = {this.handleMultifly}
        handleReset = {this.handleReset}
        />
      </React.Fragment>
      
     );
  }
}
 
export default App;